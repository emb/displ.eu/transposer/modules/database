import os
import sys
import types
import typing
import logging

from deepmerge import always_merger

import pymongo
from pymongo import MongoClient
from pymongo.errors import ConnectionFailure

from database.db_base import DBBase

class MongoDB(DBBase):
    slots = (
        '_client',
        '_connection',
    )
    
    def __init__(self, config: dict | None = None, **kwargs: typing.Any) -> None:
        super().__init__(config)
        
        logging.debug('Create MongoDB object.')
        
        self._client = self._get_client(**kwargs)
        self._client.admin.command('ping')
        
        self._connection = None


    def connect(self) -> None:
        """Connect to the database."""
        if self._client is None:
            self._client = self._get_client()
        if self._connection is not None:
            return
        if self._config['database']:
            self._connection = self._client[self._config['database']]
    
    def disconnect(self) -> None:
        """Disconnect from the database."""
        if self._connection is None:
            return
        self._connection = None
    
    def dispose(self) -> None:
        """Dispose of the database connection."""
        self.disconnect()
        if self._client is None:
            return
        self._client.close()
        self._client = None



    @property
    def module(self) -> pymongo:
        return pymongo
    
    @property
    def client(self) -> MongoClient | None:
        return self._client
    
    @property
    def connection(self) -> pymongo.database.Database | None:
        return self._connection
    
    
    def get_collection(self, name: str) -> pymongo.collection.Collection | None:
        if self._connection is None:
            return None
        return self._connection[name]
    
    
    def collection_names(self) -> list[str]:
        if self._connection is None:
            return []
        return self._connection.list_collection_names()
    
    
    def has_collection(self, name: str) -> bool | None:
        if self._connection is None:
            return None
        return name in self._connection.list_collection_names()



    # Configuration
    
    def _defaults(self) -> dict:
        """Returns a dict of default values for the config."""
        return always_merger.merge(super()._defaults(), {
            'host': 'localhost',
            'port': 27017,
            'username': None,
            'password': None,
            'database': None,
            'is_admin': False
        })


    def _config_schema(self) -> dict:
        """Returns the schema for the config dict."""
        return always_merger.merge(super()._config_schema(), {
            'username': { 'type': 'string', 'nullable': True },
            'password': { 'type': 'string', 'nullable': True },
            'host': { 'type': 'string' },
            'port': { 'type': 'integer', 'min': 0, 'max': 65535, 'default': 27017, 'coerce': int },
            'database': { 'type': 'string', 'nullable': True },
            'is_admin': { 'type': 'boolean', 'default': False },
        })


    def _get_url_object(self) -> str:
        """Returns the URL string for the database connection."""
        container = [ 'mongodb://' ]
        if self._config['username']:
            container.append(self._config['username'])
            container.append(':')
            container.append(self._config['password'])
            container.append('@')
        container.append(self._config['host'])
        container.append('/')
        if not self._config['is_admin'] and self._config['database']:
            container.append(self._config['database'])
        
        return ''.join(container)


    def _get_client(self, **kwargs: typing.Any) -> MongoClient:
        """Returns the client object for the database connection."""
        
        print('_get_client ... url_object: ', self._get_url_object())
        
        return MongoClient(self._get_url_object())





####################################################################################################
# Testing

if __name__ == '__main__':
    from dotenv import load_dotenv
    load_dotenv()
    
    config = {
        'username': os.getenv('MONGO_USERNAME'),
        'password': os.getenv('MONGO_PASSWORD'),
        'host': os.getenv('MONGO_HOST'),
        'database': os.getenv('MONGO_DATABASE'),
    }
    if os.getenv('SQLITE_DATABASE'):
        config['port'] = int(os.getenv('MONGO_PORT'))
    
    print('Creating database object ...')
    db = MongoDB(config)
    
    print('Database config:', db.config)
    print('Database url_object:', db._get_url_object())
    print('Database client:', db.client)
    
    print('Connecting ...')
    db.connect()
    
    print('Connection:', db.connection)
    
    print('Creating collection ...')
    collection = db.get_collection('test_collection_1')
    
    print('Collection:', collection)
    
    mydoc = { "name": "John", "address": "Highway 37" }
    
    print('Source doc:', mydoc)
    
    print('Writing data ...')
    result = collection.insert_one(mydoc)
    
    print('Write result:', result)
    print('Write result ID:', result.inserted_id)

    print('Reading data ...')
    items = collection.find()
    for item in items:
        print(item)
    
    print('Dropping collection ...')
    collection.drop()

    print('Disconnecting ...')
    db.disconnect()

    print('Connection:', db.connection)
    
    print('Disposing ...')
    db.dispose()
    
    print('Done')

