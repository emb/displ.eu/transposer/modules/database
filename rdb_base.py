import os
import sys
import types
import typing
import logging

from deepmerge import always_merger
import sqlalchemy

from database.db_base import DBBase

class RDBBase(DBBase):
    slots = (
        '_client',
        '_connection',
    )
    
    def __init__(self, config: dict | None = None, **kwargs: typing.Any) -> None:
        super().__init__(config)
        
        logging.debug('Create RDBBase object.')
        
        self._client = self._get_client(**kwargs)
        self._connection = None


    def connect(self) -> None:
        """Connect to the database."""
        if self._client is None:
            self._client = self._get_client()
        if self._connection is not None:
            return
        self._connection = self._client.connect()
    
    def disconnect(self) -> None:
        """Disconnect from the database."""
        if self._connection is None:
            return
        self._connection.close()
        self._connection = None
    
    def dispose(self) -> None:
        """Dispose of the database connection."""
        self.disconnect()
        if self._client is None:
            return
        self._client.dispose()
        self._client = None


    @property
    def module(self) -> sqlalchemy:
        """Returns the sqlalchemy module."""
        return sqlalchemy
    
    @property
    def client(self) -> sqlalchemy.engine.Engine | None:
        """Returns the client/engine object for the database connection."""
        return self._client
    
    @property
    def connection(self) -> sqlalchemy.engine.Connection | None:
        """Returns the connection object for the database connection."""
        return self._connection


    # Wrappers
    def execute(self, *args: typing.Any, **kwargs: typing.Any) -> sqlalchemy.engine.CursorResult:
        """Executes a query."""
        return self._connection.execute(*args, **kwargs)
    
    def execute_scalar(self, *args: typing.Any, **kwargs: typing.Any) -> typing.Any:
        """Executes a query and returns a single value."""
        return self._connection.execute_scalar(*args, **kwargs)
    
    def execute_many(self, *args: typing.Any, **kwargs: typing.Any) -> sqlalchemy.engine.CursorResult:
        """Executes a query multiple times."""
        return self._connection.execute_many(*args, **kwargs)
    
    def execute_script(self, *args: typing.Any, **kwargs: typing.Any) -> sqlalchemy.engine.CursorResult:
        """Executes a script."""
        return self._connection.execute_script(*args, **kwargs)
    
    def execute_script_file(self, *args: typing.Any, **kwargs: typing.Any) -> sqlalchemy.engine.CursorResult:
        """Executes a script file."""
        return self._connection.execute_script_file(*args, **kwargs)
    
    def execute_script_files(self, *args: typing.Any, **kwargs: typing.Any) -> sqlalchemy.engine.CursorResult:
        """Executes multiple script files."""
        return self._connection.execute_script_files(*args, **kwargs)
    
    def execute_script_directory(self, *args: typing.Any, **kwargs: typing.Any) -> sqlalchemy.engine.CursorResult:
        """Executes all script files in a directory."""
        return self._connection.execute_script_directory(*args, **kwargs)
    
    def execute_script_directories(self, *args: typing.Any, **kwargs: typing.Any) -> sqlalchemy.engine.CursorResult:
        """Executes all script files in multiple directories."""
        return self._connection.execute_script_directories(*args, **kwargs)
    
    def execute_script_recursive(self, *args: typing.Any, **kwargs: typing.Any) -> sqlalchemy.engine.CursorResult:
        """Executes all script files in a directory recursively."""
        return self._connection.execute_script_recursive(*args, **kwargs)
    
    def execute_script_recursives(self, *args: typing.Any, **kwargs: typing.Any) -> sqlalchemy.engine.CursorResult:
        """Executes all script files in multiple directories recursively."""
        return self._connection.execute_script_recursives(*args, **kwargs)
    
    def execute_script_string(self, *args: typing.Any, **kwargs: typing.Any) -> sqlalchemy.engine.CursorResult:
        """Executes a script string."""
        return self._connection.execute_script_string(*args, **kwargs)
    
    def execute_script_strings(self, *args: typing.Any, **kwargs: typing.Any) -> sqlalchemy.engine.CursorResult:
        """Executes multiple script strings."""
        return self._connection.execute_script_strings(*args, **kwargs)
    
    
    #def insert(self, *args: typing.Any, **kwargs: typing.Any) -> sqlalchemy.engine.CursorResult:
    #    """Inserts a row."""
    #    return self._connection.insert(*args, **kwargs)
    
    
    def get_table(self, *args: typing.Any, **kwargs: typing.Any) -> sqlalchemy.Table:
        """Returns a table object."""
        return self._connection.get_table(*args, **kwargs)
    
    def get_tables(self, *args: typing.Any, **kwargs: typing.Any) -> typing.List[sqlalchemy.Table]:
        """Returns a list of table objects."""
        return self._connection.get_tables(*args, **kwargs)
    
    def get_view(self, *args: typing.Any, **kwargs: typing.Any) -> sqlalchemy.Table:
        """Returns a view object."""
        return self._connection.get_view(*args, **kwargs)
    
    def get_views(self, *args: typing.Any, **kwargs: typing.Any) -> typing.List[sqlalchemy.Table]:
        """Returns a list of view objects."""
        return self._connection.get_views(*args, **kwargs)
    
    def get_table_or_view(self, *args: typing.Any, **kwargs: typing.Any) -> sqlalchemy.Table:
        """Returns a table or view object."""
        return self._connection.get_table_or_view(*args, **kwargs)
    
    def get_tables_or_views(self, *args: typing.Any, **kwargs: typing.Any) -> typing.List[sqlalchemy.Table]:
        """Returns a list of table or view objects."""
        return self._connection.get_tables_or_views(*args, **kwargs)
    
    def get_table_or_view_by_name(self, *args: typing.Any, **kwargs: typing.Any) -> sqlalchemy.Table:
        """Returns a table or view object by name."""
        return self._connection.get_table_or_view_by_name(*args, **kwargs)
    
    def get_tables_or_views_by_name(self, *args: typing.Any, **kwargs: typing.Any) -> typing.List[sqlalchemy.Table]:
        """Returns a list of table or view objects by name."""
        return self._connection.get_tables_or_views_by_name(*args, **kwargs)
    
    def get_table_by_name(self, *args: typing.Any, **kwargs: typing.Any) -> sqlalchemy.Table:
        """Returns a table object by name."""
        return self._connection.get_table_by_name(*args, **kwargs)
    
    def get_tables_by_name(self, *args: typing.Any, **kwargs: typing.Any) -> typing.List[sqlalchemy.Table]:
        """Returns a list of table objects by name."""
        return self._connection.get_tables_by_name(*args, **kwargs)
    
    def get_view_by_name(self, *args: typing.Any, **kwargs: typing.Any) -> sqlalchemy.Table:
        """Returns a view object by name."""
        return self._connection.get_view_by_name(*args, **kwargs)
    
    def get_views_by_name(self, *args: typing.Any, **kwargs: typing.Any) -> typing.List[sqlalchemy.Table]:
        """Returns a list of view objects by name."""
        return self._connection.get_views_by_name(*args, **kwargs)
    
    def get_table_or_view_by_name(self, *args: typing.Any, **kwargs: typing.Any) -> sqlalchemy.Table:
        """Returns a table or view object by name."""
        return self._connection.get_table_or_view_by_name(*args, **kwargs)
    
    
    @property
    def Table(self):
        """Returns the Table class."""
        return sqlalchemy.schema.Table
    
    @property
    def Column(self):
        """Returns the Column class."""
        return sqlalchemy.schema.Column
    
    @property
    def MetaData(self):
        """Returns the MetaData class."""
        return sqlalchemy.schema.MetaData
    
    @property
    def SchemaConst(self):
        """Returns the SchemaConst class."""
        return sqlalchemy.schema.SchemaConst
    
    @property
    def SchemaItem(self):
        """Returns the SchemaItem class."""
        return sqlalchemy.schema.SchemaItem
    
    
    
    @property
    def schema(self):
        """Returns the schema sub-module."""
        return sqlalchemy.schema
    
    @property
    def sql(self):
        """Returns the sql sub-module."""
        return sqlalchemy.sql
    
    @property
    def sqlexpr(self):
        """Returns the sql sub-module."""
        return sqlalchemy.sql.expression
    
    @property
    def compiler(self):
        """Returns the compiler sub-module."""
        return sqlalchemy.sql.compiler
    
    @property
    def orm(self):
        """Returns the orm sub-module."""
        return sqlalchemy.orm
    
    @property
    def engine(self):
        """Returns the engine sub-module."""
        return sqlalchemy.engine
    
    @property
    def reflection(self):
        """Returns the reflection sub-module."""
        return sqlalchemy.engine.reflection
    
    @property
    def interfaces(self):
        """Returns the interfaces sub-module."""
        return sqlalchemy.engine.interfaces
    
    @property
    def exc(self):
        """Returns the exc sub-module."""
        return sqlalchemy.exc
    
    @property
    def types(self):
        """Returns the types sub-module."""
        return sqlalchemy.types
    
    @property
    def engines(self):
        """Returns the clickhouse engines."""
        return None
    

    
    @property
    def create_engine(self):
        """Returns the create_engine function."""
        return sqlalchemy.schema.create_engine
    
    @property
    def make_session(self):
        """Returns the make_session function."""
        return None
    
    @property
    def insert_sentinel(self):
        """Returns the insert_sentinel function."""
        return sqlalchemy.schema.insert_sentinel
    
    @property
    def literal(self):
        """Returns the literal function."""
        return sqlalchemy.literal
    


    # Configuration

    def _defaults(self) -> dict:
        """Returns a dict of default values for the config."""
        return always_merger.merge(super()._defaults(), {
            
        })


    def _config_schema(self) -> dict:
        """Returns the schema for the config dict."""
        return always_merger.merge(super()._config_schema(), {
            'dialect': { 'type': 'string', 'allowed': ['sqlite', 'mysql', 'postgresql', 'oracle', 'mssql'] },
            'driver': { 'type': 'string', 'allowed': ['pysqlite', 'mysqldb', 'pymysql', 'psycopg2', 'cx_oracle', 'pyodbc'] },
            'database': { 'type': 'string' },
        })


    def _get_url_object(self) -> sqlalchemy.engine.URL:
        """Returns the URL object for the database connection."""
        return sqlalchemy.engine.URL.create(
            f"{self._config['dialect']}+{self._config['driver']}"
        )


    def _get_client(self, **kwargs: typing.Any) -> sqlalchemy.engine.Engine:
        """Returns the client object for the database connection."""
        return sqlalchemy.create_engine(
            self._get_url_object(),
            **kwargs
        )

