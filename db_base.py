import os
import sys
import types
import typing
import logging

from deepmerge import always_merger
from cerberus import Validator

class DBBase(object):
    slots = (
        '_config',
        '_schema_validator'
    )
    
    def __init__(self, config: dict | None = None) -> None:
        super().__init__()
        
        logging.debug('Create DBBase object.')
        
        # Create the schema validator.
        self._schema_validator = Validator(
            self._config_schema(),
            allow_unknown=True
        )
        
        # Set the config.
        self.config = config


    @property
    def config(self) -> dict:
        """Returns the config dict."""
        return self._config
    
    @config.setter
    def config(self, value: dict) -> None:
        """Sets the config dict."""
        # Merge the given config with the default config.
        value = self._merge_config(value)
        
        # Validate the config.
        if self._schema_validator.validate(value) is False:
            logging.error(self._schema_validator.errors)
            raise ValueError(self._schema_validator.errors)
        
        # Set the config.
        self._config = value


    def connect(self) -> None:
        """Connect to the database."""
        raise NotImplementedError()
    
    def disconnect(self) -> None:
        """Disconnect from the database."""
        raise NotImplementedError()
    
    def dispose(self) -> None:
        """Dispose of the database connection.
        This is used to close the connection pool.
        This is used by SQLAlchemy, but we define it here,
        so we establish a common interface for all databases."""
        pass
    

    def _defaults(self) -> dict:
        """Returns a dict of default values for the config."""
        return {}

    def _config_schema(self) -> dict:
        """Returns the schema for the config dict."""
        return {}

    def _merge_config(self, value: dict | None = None) -> dict:
        """Merge the given config with the default config."""
        if value is None:
            return self._defaults()
        return always_merger.merge(self._defaults(), value)

