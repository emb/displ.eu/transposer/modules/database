# Transposer Database

Collection of database abstractions for the following databases:
* SQLite
* MySQL
* PostgreSQL
* MongoDB

The abstractions offer a high-level interface that homogenize the use of the databases as much as possible.


## Install

To install, use the *Shared Library (shared-lib)* template project and clone this one into the `modules` directory. If you follow the installation instructions of the service(s) you intend to use and `shared-lib`, the service(s) will pick up the module automatically.

### Dependencies

To install the dependencies,
```bash
pip install -r requirements.txt
```
inside each service's virtual environment that uses the `shared-lib` this is installed into.
