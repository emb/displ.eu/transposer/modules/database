import os
import sys
import types
import typing
import logging

from deepmerge import always_merger
import sqlalchemy

from database.rdb_base import RDBBase

class SQLite(RDBBase):
    
    def __init__(self, config: dict | None = None, **kwargs: typing.Any) -> None:
        super().__init__(config, **kwargs)
        logging.debug('Create SQLite object.')



    # Configuration
    
    def _defaults(self) -> dict:
        """Returns a dict of default values for the config."""
        return always_merger.merge(super()._defaults(), {
            'dialect': 'sqlite',
            'driver': 'pysqlite',
            'database': ':memory:',
        })


    def _config_schema(self) -> dict:
        """Returns the schema for the config dict."""
        return always_merger.merge(super()._config_schema(), {
            'dialect': { 'type': 'string', 'allowed': ['sqlite'] },
            'driver': { 'type': 'string', 'allowed': ['pysqlite'] },
            'database': { 'type': 'string' },
        })


    def _get_url_object(self) -> sqlalchemy.engine.URL:
        """Returns the URL object for the database connection."""
        return sqlalchemy.engine.URL.create(
            f"{self._config['dialect']}+{self._config['driver']}",
            database=self._config['database'],
        )





####################################################################################################
# Testing

if __name__ == '__main__':
    from dotenv import load_dotenv
    load_dotenv()
    
    config = None
    if os.getenv('SQLITE_DATABASE'):
        config = {
            'database': os.getenv('SQLITE_DATABASE')
        }
    
    print('Creating database object ...')
    db = SQLite(config)
    
    print('Database config:', db.config)
    print('Database url_object:', db._get_url_object())
    print('Database client:', db.client)
    
    print('Connecting ...')
    db.connect()
    
    print('Connection:', db.connection)

    client = db.client
    metadata = db.MetaData()
    print('Client:', client)
    print('MetaData:', metadata)

    print('Creating table ...')
    some_table = db.Table('some_table', metadata,
        db.Column('id', db.types.Integer, primary_key=True),
        db.Column('name', db.types.String),
        db.Column('age', db.types.Integer),
        db.Column('weight', db.types.Float),
        db.Column('is_human', db.types.Boolean)
    )
    print('Table:', some_table)
    
    metadata.create_all(client)
    
    print('Creating query ...')
    query = db.module.insert(some_table).values(id=1, name='Matthew', age=21, weight=85, is_human=True)
    print('Query:', query)
    
    print('Writing data ...')
    result = db.execute(query)
    print('Result:', result)

    print('Reading data ...')
    output = db.execute(some_table.select()).fetchall()
    print('Output:', output)
    
    #print('Dropping table ...')

    print('Disconnecting ...')
    db.disconnect()

    print('Connection:', db.connection)
    
    print('Disposing ...')
    db.dispose()
    
    print('Done')

