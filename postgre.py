import os
import sys
import types
import typing
import logging

from deepmerge import always_merger
import sqlalchemy

from database.rdb_base import RDBBase

class PostgreSQL(RDBBase):
    
    def __init__(self, config: dict | None = None, **kwargs: typing.Any) -> None:
        super().__init__(config, **kwargs)
        logging.debug('Create PostgreSQL object.')



    # Configuration
    
    def _defaults(self) -> dict:
        """Returns a dict of default values for the config."""
        return always_merger.merge(super()._defaults(), {
            'dialect': 'postgresql',
            'driver': 'psycopg2',
            'host': 'localhost',
            'port': 5432,
        })


    def _config_schema(self) -> dict:
        """Returns the schema for the config dict."""
        return always_merger.merge(super()._config_schema(), {
            'dialect': { 'type': 'string', 'allowed': ['postgresql'] },
            'driver': { 'type': 'string', 'allowed': ['psycopg2'] },
            'username': { 'type': 'string', 'required': True },
            'password': { 'type': 'string', 'required': True },
            'host': { 'type': 'string' },
            'port': { 'type': 'integer', 'min': 0, 'max': 65535, 'default': 5432, 'coerce': int },
            'database': { 'type': 'string', 'required': True },
        })


    def _get_url_object(self) -> sqlalchemy.engine.URL:
        """Returns the URL object for the database connection."""
        return sqlalchemy.engine.URL.create(
            f"{self._config['dialect']}+{self._config['driver']}",
            username=self._config['username'],
            password=self._config['password'],
            host=self._config['host'],
            port=self._config['port'],
            database=self._config['database'],
        )





####################################################################################################
# Testing

if __name__ == '__main__':
    from dotenv import load_dotenv
    load_dotenv()
    
    config = {
        'username': os.getenv('POSTGRES_USERNAME'),
        'password': os.getenv('POSTGRES_PASSWORD'),
        'host': os.getenv('POSTGRES_HOST'),
        'database': os.getenv('POSTGRES_DATABASE'),
    }
    if os.getenv('SQLITE_DATABASE'):
        config['port'] = int(os.getenv('POSTGRES_PORT'))
    
    print('Creating database object ...')
    db = PostgreSQL(config)
    
    print('Database config:', db.config)
    print('Database url_object:', db._get_url_object())
    print('Database client:', db.client)
    
    print('Connecting ...')
    db.connect()
    
    print('Connection:', db.connection)

    client = db.client
    metadata = db.MetaData()
    print('Client:', client)
    print('MetaData:', metadata)

    print('Creating table ...')
    some_table = db.Table('some_table', metadata,
        db.Column('id', db.types.Integer, primary_key=True),
        db.Column('name', db.types.String(64)),
        db.Column('age', db.types.Integer),
        db.Column('weight', db.types.Float),
        db.Column('is_human', db.types.Boolean)
    )
    print('Table:', some_table)
    
    metadata.create_all(client)
    
    print('Creating query ...')
    query = db.module.insert(some_table).values(id=1, name='Matthew', age=21, weight=85, is_human=True)
    print('Query:', query)
    
    print('Writing data ...')
    with client.begin() as conn:
        result = conn.execute(query)
        print('result:', result)
    
    print('Reading data ...')
    with client.begin() as conn:
        output = conn.execute(some_table.select()).fetchall()
        print('output:', output)
    
    print('Dropping table ...')
    some_table.drop(client)
    
    print('Disconnecting ...')
    db.disconnect()

    print('Connection:', db.connection)
    
    print('Disposing ...')
    db.dispose()
    
    print('Done')

